class DesasociarCultivosController < ApplicationController
  # GET /desasociar_cultivos
  # GET /desasociar_cultivos.json

  def index
    @sensor_viveros = SensorVivero.joins(:users, :perfils).where("users.id = :userid AND perfils.perfil in (:perfiles) AND sensor_viveros.cultivo_id is not NULL", userid: current_user.id, perfiles: ['Propietario', 'Agricultor']).map{ |x| [x.nombre, x.id]}

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sensor_viveros }
    end
  end

  # PUT /desasociar_cultivos
  # PUT /desasociar_cultivos.json
  def update
    @sensor_vivero = SensorVivero.find(params[:desasociar][:sensor_vivero_id])

    respond_to do |format|
      if @sensor_vivero.update_attribute(:cultivo_id, nil)
        format.html { redirect_to @sensor_vivero, notice: 'Sistema liberado correctamente.'}
        format.json { head :no_content }
      else
        format.html { redirect_to @sensor_vivero, notice: 'Ha ocurrido un error.' }
        format.json { head :no_content }
      end
    end
  end
end
