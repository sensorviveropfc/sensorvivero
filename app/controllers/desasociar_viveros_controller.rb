class DesasociarViverosController < ApplicationController
  # GET /desasociar_viveros
  # GET /desasociar_viveros.json
  def index
    @viveros = Vivero.joins(:users).where("users.id = :userid AND users.role = :role", userid: current_user.id, role: 'admin').map{ |x| [x.nombre, x.id]}

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @viveros }
    end
  end

  # PUT /desasociar_viveros
  # PUT /desasociar_viveros.json
  def update
    @vivero = Vivero.find(params[:desasociar_vivero][:vivero_id])
    @user = User.where("users.email = :mail", mail: params[:desasociar_vivero][:user_mail])

    respond_to do |format|

      if @user.empty?
        format.html { redirect_to viveros_path, notice: 'El usuario no existe.' }
        format.json { head :no_content }
      elsif not @vivero.users.include?(@user[0])
        format.html { redirect_to viveros_path, notice: 'El usuario no estaba asociado al vivero.' }
        format.json { head :no_content }
      else
        @vivero.users.delete(@user[0])
        if @vivero.save
          format.html { redirect_to viveros_path, notice: 'Usuario dado de baja del vivero correctamente.'}
          format.json { head :no_content }
        else
          format.html { redirect_to viveros_path, notice: 'Ha ocurrido un error.' }
          format.json { head :no_content }
        end
      end

    end
  end
end
