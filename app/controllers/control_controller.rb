class ControlController < ApplicationController
  before_filter :authenticate_user!
  def principal

    #Ejemplo para enviar un correo
    #  UserMailer.send_email("miguepiscy@gmail.com","funcionaaaaa","este es el cuerpo del email").deliver

    @sistemas = current_user.sensor_viveros
  #  @cultivos_sin_asociar = Cultivos.where(numero_serie: numSerie)
  end

  def control

    @sensor_propios = SensorVivero.joins(:users, :perfils).where("users.id = :userid AND perfils.perfil in (:perfiles) AND sensor_viveros.cultivo_id is not NULL", userid: current_user.id, perfiles: ['Propietario', 'Agricultor'])

    @sistema = SensorVivero.find_by_id(params[:sistema_id])
    if @sensor_propios.include?(@sistema)
      @cultivo = @sistema.cultivo

      max = 0
      acu = 0

      if @cultivo.maximos_pulsos_riego != nil
      max = @cultivo.maximos_pulsos_riego
      end

      if @cultivo.pulsos_riego_acumulados != nil
      acu = @cultivo.pulsos_riego_acumulados
      end

      diferencia = max - acu
      @pulsos = diferencia < 0 ? 0 : diferencia

    else
      @sistema = nil
    end

  end

  def return_control
    numSerie = params[:parametros][:numero_serie]
    sensorv = SensorVivero.where(numero_serie: numSerie).first

    if sensorv

      @temperatura = params[:parametros][:temperatura]
      @luz = params[:parametros][:luz]
      @riego = params[:parametros][:riego]
      @modo = params[:parametros][:modo]
      @fotoperiodo = params[:parametros][:fotoperiodo]
      @luz_limite = params[:parametros][:luz_limite]
      @temp_minima = params[:parametros][:temperatura_minima]
      @temp_maxima = params[:parametros][:temperatura_maxima]
      @max_pulsos_riego = params[:parametros][:maximos_pulsos_riego]
      @humedad_minima = params[:parametros][:humedad_minima]
      @humedad_maxima = params[:parametros][:humedad_maxima]

      array = Hash.new
      array = { "modo" => @modo,"temperatura" => @temperatura,"luz" => @luz, "riego" => @riego,
        "fotoperiodo" => @fotoperiodo, "luz_limite" => @luz_limite, "temp_minima" => @temp_minima,
        "temp_maxima" => @temp_maxima, "max_pulsos_riego" => @max_pulsos_riego, "humedad_minima" => @humedad_minima,
        "humedad_maxima" => @humedad_maxima }

      puts "Cache anterior "
      puts Rails.cache.read(numSerie)
      Rails.cache.delete(numSerie)
      Rails.cache.write(numSerie, array)
      puts "cambio de estado para "+ numSerie.to_s
      puts "Cache actual "
      puts Rails.cache.read(numSerie)

      sensorv.update_attributes(estado_actualizado: false)
    end

  end

end
