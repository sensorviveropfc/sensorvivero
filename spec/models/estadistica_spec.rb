require 'spec_helper'

describe Estadistica do

	it "Comprobar que no se crea una estadistica sin cultivo" do 
		assert_raise(ActiveRecord::StatementInvalid){FactoryGirl.create(:estadistica)}  	
	end

	it "Comprobar que se crea una estadistica bien con un cultivo" do 
		vivero = FactoryGirl.create(:vivero_prueba)
       	cultivo = FactoryGirl.create(:cultivo,vivero_id: vivero.id)
	  	estadistica = FactoryGirl.create(:estadistica,cultivo_id: cultivo.id)

	  	assert_equal(cultivo.id,estadistica.cultivo_id)
	end
end
