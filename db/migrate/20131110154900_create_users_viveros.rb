class CreateUsersViveros < ActiveRecord::Migration
  def change
    create_table :users_viveros, :id => false do |t|

      t.references :user
      t.references :vivero

    end

    add_foreign_key :users_viveros, :users, dependent: :delete
    add_foreign_key :users_viveros, :viveros, dependent: :delete

  end
end