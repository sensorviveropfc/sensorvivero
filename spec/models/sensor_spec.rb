require 'spec_helper'

describe Sensor do

  it "comprobar que  no se crea un sensor sin un sensor vivero" do
  	assert_raise(ActiveRecord::StatementInvalid){FactoryGirl.create(:sensor)}
  end

  it "comprobar que se crea un sensor con un sensor vivero" do
  	sensorvivero =  FactoryGirl.create(:sensor_vivero_pruebas)
  	sensor = FactoryGirl.create(:sensor, sensor_vivero_id: sensorvivero.id )
	assert_equal(sensorvivero.id, sensor.sensor_vivero_id)
  end
end
