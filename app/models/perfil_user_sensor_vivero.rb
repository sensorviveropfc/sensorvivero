class PerfilUserSensorVivero < ActiveRecord::Base
  belongs_to :perfil
  belongs_to :user
  belongs_to :sensor_vivero
  attr_accessible :perfil, :user, :sensor_vivero
end
