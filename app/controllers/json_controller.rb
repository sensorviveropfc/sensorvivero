class JsonController < ApplicationController
  require 'json'

  def datos
    puts "PARAMSSSS "+params.to_s
    if params[:info]
      entrada = request.body.read
      info = ActiveSupport::JSON.decode(entrada)
      num_serie = info["info"]["num_serie"].to_s
      estado = info["info"]["estado"]
      
      cache = Rails.cache.read(num_serie)
      
      ack = nil
      luminosidad = nil
      humedad = nil
      temperatura = nil
      luzActiva = nil
      riegoActivo = nil
      tempAlertActiva = nil
      lumMin = nil
      lumMax = nil
      humMin = nil
      humMax = nil
      tempMin = nil
      tempMax = nil
      fotoPeriodo = nil
      maxPulsosRiego = nil
      nuevosPulsos = 0
      error = nil
      
      sensorv = SensorVivero.where(numero_serie: num_serie).first
      cultivo = nil
      if sensorv
        cultivo = sensorv.cultivo
      end
      
      if cultivo
        luzActiva = sensorv.luz_encendida
        riegoActivo = sensorv.riego_activo
        tempAlertActiva = sensorv.ventilacion_encendida
        lumMin = cultivo.luz_limite
        lumMax = cultivo.luz_limite
        humMin = cultivo.humedad_minima
        humMax = cultivo.humedad_maxima
        tempMin = cultivo.temperatura_minima
        tempMax = cultivo.temperatura_maxima
        fotoPeriodo = cultivo.fotoperiodo
        maxPulsosRiego = cultivo.maximos_pulsos_riego
        pulsosAcumulados = cultivo.pulsos_riego_acumulados
        nuevosPulsos = 0
        
        if cache and cache["riego"]
          nuevosPulsos = 0
          riego = cache["riego"]
          for i in (1..riego.to_i).step(10)
            nuevosPulsos = nuevosPulsos + 1
          end
          pulsosAcumulados = pulsosAcumulados + nuevosPulsos
        end
      end

      estado.each do |array|
        elemento = array["e"].to_s
        valor = array["v"].to_i
        valorS = array["v"]

        case elemento
        when 'Luminosidad'
          luminosidad = valor
        when 'Humedad'
          humedad = valor
        when 'Temperatura'
          temperatura = valor
        when 'LuzActiva'
          luzActiva = !valor.zero?
        when 'RiegoActivo'
          riegoActivo = !valor.zero?
        when 'TempActiva'
          tempAlertActiva = !valor.zero?
        when 'NuevoModo'
          modo = valorS
        when 'NumSerie'
          ack = true
        when 'LumMin'
          lumMin = valor
        when 'LumMax'
          lumMax = valor
        when 'HumMin'
          humMin = valor
        when 'HumMax'
          humMax = valor
        when 'TempMin'
          tempMin = valor
        when 'TempMax'
          tempMax = valor
        when 'FotoPeriodo'
          fotoPeriodo = valor
        when 'MaxPulsosRiego'
          maxPulsosRiego = valor
        when 'error'
          error = valorS
        end        
      end
      
      if ack
          #Arduino ha ejecutado las acciones. Todo ok.
          if sensorv and cultivo
            sensorv.update_attributes(luz_encendida: luzActiva,riego_activo: riegoActivo,ventilacion_encendida: tempAlertActiva)
            cultivo.update_attributes(luz_limite: lumMin, luz_limite: lumMin, humedad_minima: 
                    humMin,humedad_maxima: humMax,temperatura_minima: tempMin,  temperatura_maxima: tempMax, 
                    fotoperiodo: fotoPeriodo, maximos_pulsos_riego: maxPulsosRiego, pulsos_riego_acumulados: pulsosAcumulados)

            Rails.cache.delete(num_serie)
            puts 'CACHE BORRADA'
            sensorv.update_attributes(estado_actualizado: true)
            # Será necesario volver a responder
            respuesta = Array.new
            respuesta << nop
            render :json => {:acciones => respuesta}
          end
      
      else
        #Arduino inicia la conversación. Gestionamos  los datos y enviamos las ordenes.
        gestionardatos(num_serie,luminosidad,humedad,temperatura, luzActiva,riegoActivo,tempAlertActiva)
  
        if cache
          respuesta = getRespuesta(num_serie, cache)
          puts respuesta
          render :json => { :acc => respuesta}
        else
          respuesta = Array.new
          respuesta << nop
          puts respuesta
          Rails.cache.delete(num_serie)
          render :json => {:acc => respuesta}
        end
      end
      
      if error
        puts "Ha ocurrido un error:"
        puts error.to_s
      end
      
    else
      render :json => {:mensaje => "Reenviar datos"}
    end
  end

  def getRespuesta(num_serie, cache)
    
    sensorv = SensorVivero.where(numero_serie: num_serie).first
    if not sensorv
      resultado << nop
      return resultado
    else
      modo = cache["modo"]
      temperatura = cache["temperatura"]
      luz = cache["luz"]
      riego = cache["riego"]    
      fotoperiodo = cache["fotoperiodo"]
      luz_limite = cache["luz_limite"]
      temp_minima = cache["temperatura_minima"]
      temp_maxima = cache["temperatura_maxima"]
      max_pulsos_riego = cache["max_pulsos_riego"]
      humedad_minima = cache["humedad_minima"]
      humedad_maxima = cache["humedad_maxima"]  
      
      resultado = Array.new
      if modo
        if modo == 'auto'
          resultado << switchModeAuto
          return resultado
        elsif modo == 'manual'
          resultado << switchModeManual
        else
          resultado << switchModeAsist
        end
      end
  
      if luz
        if luz.to_i == 1
          resultado << encenderLuz
        else
          resultado << apagarLuz
        end
      end
      
      if temperatura
        if temperatura.to_i == 1
          resultado << encenderTemp
        else
          resultado << apagarTemp
        end
      end
  
      if riego
        if riego.to_i > 0
          resultado << encenderRiego
          for i in (1..riego.to_i).step(10)
            resultado << delay10000
          end
          resultado << apagarRiego
        end
      end 

      if fotoperiodo and not fotoperiodo.to_i == sensorv.cultivo.fotoperiodo
        resultado << setFotoPeriodo(fotoperiodo)
      end
      
      if luz_limite and not luz_limite.to_i == sensorv.cultivo.luz_limite
        resultado << setLumMinMax(luz_limite, luz_limite)
      end
      
      if temp_minima and temp_maxima and (not temp_minima.to_i == sensorv.cultivo.temperatura_minima or not temp_maxima.to_i == sensorv.cultivo.temperatura_maxima)
        resultado << setTempMinMax(temp_minima,temp_maxima)
      end
      
      if max_pulsos_riego and not max_pulsos_riego.to_i == sensorv.cultivo.maximos_pulsos_riego
        resultado << setMaxPulsosRiegos(max_pulsos_riego)
      end
      
      if humedad_minima and humedad_maxima and (not humedad_minima.to_i == sensorv.cultivo.humedad_minima or not humedad_maxima.to_i == sensorv.cultivo.humedad_maxima)
        resultado << setHumMinMax(humedad_minima,humedad_maxima)
      end
    end
            
    return resultado
  end

  def gestionardatos(numSerie,luminosidad,humedad,temperatura,luzActiva,riegoActivo,tempAlertActiva)
    sensorv = SensorVivero.where(numero_serie: numSerie).first
    if sensorv
      sensorv.update_attributes(luz_encendida: luzActiva,riego_activo: riegoActivo,ventilacion_encendida: tempAlertActiva)
      cultivo = sensorv.cultivo
      if cultivo
        cultivo.update_attributes(luz_actual: luminosidad,humedad_actual: humedad, temperatura_actual: temperatura)

        #TODO por ahora se pasa co2 y ph a nil. En el futuro si se usan estos parametros habrá que pasarle los valores adecuados.
        gestionarAlertasCorreo(cultivo, temperatura, humedad, luminosidad, nil, nil)
        
        grabarMedida(cultivo, temperatura, humedad, luminosidad, nil, nil)

      else
      #¡¡¡error!!! un sistema sin cultivo asociado esta enviando datos.
      end
    end
  end

  
  #Send an alert if any parameter is outside the limits.
  def gestionarAlertasCorreo(cultivo, temperatura, humedad, luminosidad, co2, ph)

    #I note that the values ​​are integers and compared with the limits
    if (temperatura.is_a? Integer) && (cultivo.temperatura_minima.is_a? Integer) && (cultivo.temperatura_maxima.is_a? Integer) &&
       (temperatura <= cultivo.temperatura_minima || temperatura >= cultivo.temperatura_maxima)
      cultivo.vivero.users.each do |u|
        UserMailer.send_email(u.email,"Alerta de temperatura: Su cultivo necesita atencion","Se ha detectado una temperatura de #{temperatura} para su cultivo #{cultivo.nombre}").deliver
      end
    end

    #I note that the values ​​are integers and compared with the limits
    if (humedad.is_a? Integer) && (cultivo.humedad_minima.is_a? Integer)   && (cultivo.humedad_maxima.is_a? Integer) &&
      (humedad <= cultivo.humedad_minima || humedad >= cultivo.humedad_maxima)
      cultivo.vivero.users.each do |u|
        UserMailer.send_email(u.email,"Alerta de humedad: Su cultivo necesita atencion","Se ha detectado una humedad de #{humedad} para su cultivo #{cultivo.nombre}").deliver
      end
    end

    #I note that the values ​​are integers and compared with the limits
    if (co2.is_a? Integer) && (cultivo.co2_maximo.is_a? Integer) &&
      (co2 >= cultivo.co2_maximo)
      cultivo.vivero.users.each do |u|
        UserMailer.send_email(u.email,"Alerta de CO2: Su cultivo necesita atencion","Se ha detectado un nivel de CO2 de #{co2} para su cultivo #{cultivo.nombre}").deliver
      end
    end

    #I note that the values ​​are integers and compared with the limits
    if (ph.is_a? Integer) && (cultivo.ph_minimo.is_a? Integer)  && (cultivo.ph_maximo.is_a? Integer) &&
      (ph <= cultivo.ph_minimo || ph >= cultivo.ph_maximo)
      cultivo.vivero.users.each do |u|
        UserMailer.send_email(u.email,"Alerta de PH: Su cultivo necesita atencion","Se ha detectado un ph de #{ph} para su cultivo #{cultivo.nombre}").deliver
      end
    end

  end
  
  def grabarMedida(cultivo, temperatura, humedad, luminosidad, co2, ph)
    
    if cultivo.estadisticas.empty?
      estadistica = Estadistica.new(descripcion: 'Estadistica')
      cultivo.estadisticas << estadistica
      cultivo.save
    end 
     
    estadistica = cultivo.estadisticas.sort_by{|e| e.updated_at}.reverse[0]
    
    medida = Medida.new(temperatura: temperatura, luminosidad: luminosidad, humedad: humedad, co2: co2, ph: ph)  
    estadistica.medidas << medida
    estadistica.save

  end

  def switchModeManual
    return { :a => 0, :v1 => "m" }
  end

  def switchModeAsist
    return { :a => 0 , :v1 => "s" }
  end

  def switchModeAuto
    return { :a => 0, :v1 => "x" }
  end

  def getData
    return { :a => 1 }
  end

  def encenderRiego
    return { :a => 2 }
  end

  def encenderLuz
    return { :a => 3 }
  end

  def encenderTemp
    return { :a => 4 }
  end

  def apagarRiego
    return { :a => 5 }
  end

  def apagarLuz
    return { :a => 6 }
  end

  def apagarTemp
    return { :a => 7 }
  end

  def getLumMinMax
    return { :a => 8 }
  end

  def getTempMinMax
    return { :a => 9 }
  end

  def getHumMinMax
    return { :a => 10 }
  end

  def getMaxPulsosRiegos
    return { :a => 11 }
  end

  #Hist => Histeresis
  def getLumHist
    return { :a => 12 }
  end

  def getFotoPeriodo
    return { :a => 13 }
  end

  def getHoraReset
    return { :a => 14 }
  end

  def setLumMinMax(val,val2)
    return { :a => 15 ,:v1 => val, :v2 => val2 }
  end

  def setTempMinMax(val,val2)
    return { :a => 16 ,:v1 => val, :v2 => val2 }
  end

  def setHumMinMax(val,val2)
    return { :a => 17 ,:v1 => val, :v2 => val2 }
  end

  def setMaxPulsosRiegos(val)
    return { :a => 18,:v1 => val }
  end

  def setLumHist(val)
    return { :a => 19 ,:v1 => val }
  end

  def setFotoPeriodo(val)
    return { :a => 20 ,:v1 => val }
  end

  def setHoraReset(hora,minuto)
    return { :a => 21,:v1 => hora, :v2 => minuto }
  end

  def getNumSerie
    return { :a => 22 }
  end

  def delay10000
    return { :a => 23 ,:v1 => 10000 }
  end

  def nop
    return { :a => 24 }
  end

end

