require 'test_helper'

class CultivosControllerTest < ActionController::TestCase
  setup do
    @cultivo = cultivos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cultivos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cultivo" do
    assert_difference('Cultivo.count') do
      post :create, cultivo: { co2_actual: @cultivo.co2_actual, co2_maximo: @cultivo.co2_maximo, cosecha: @cultivo.cosecha, duracion_pulso_riego: @cultivo.duracion_pulso_riego, fotoperiodo: @cultivo.fotoperiodo, funcionamiento: @cultivo.funcionamiento, humedad_actual: @cultivo.humedad_actual, humedad_maxima: @cultivo.humedad_maxima, humedad_minima: @cultivo.humedad_minima, luz_actual: @cultivo.luz_actual, luz_acumulada: @cultivo.luz_acumulada, luz_limite: @cultivo.luz_limite, maximos_pulsos_riego: @cultivo.maximos_pulsos_riego, nombre: @cultivo.nombre, ph_actual: @cultivo.ph_actual, ph_maximo: @cultivo.ph_maximo, ph_minimo: @cultivo.ph_minimo, pulsos_riego_acumulados: @cultivo.pulsos_riego_acumulados, temperatura_actual: @cultivo.temperatura_actual, temperatura_maxima: @cultivo.temperatura_maxima, temperatura_minima: @cultivo.temperatura_minima }
    end

    assert_redirected_to cultivo_path(assigns(:cultivo))
  end

  test "should show cultivo" do
    get :show, id: @cultivo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cultivo
    assert_response :success
  end

  test "should update cultivo" do
    put :update, id: @cultivo, cultivo: { co2_actual: @cultivo.co2_actual, co2_maximo: @cultivo.co2_maximo, cosecha: @cultivo.cosecha, duracion_pulso_riego: @cultivo.duracion_pulso_riego, fotoperiodo: @cultivo.fotoperiodo, funcionamiento: @cultivo.funcionamiento, humedad_actual: @cultivo.humedad_actual, humedad_maxima: @cultivo.humedad_maxima, humedad_minima: @cultivo.humedad_minima, luz_actual: @cultivo.luz_actual, luz_acumulada: @cultivo.luz_acumulada, luz_limite: @cultivo.luz_limite, maximos_pulsos_riego: @cultivo.maximos_pulsos_riego, nombre: @cultivo.nombre, ph_actual: @cultivo.ph_actual, ph_maximo: @cultivo.ph_maximo, ph_minimo: @cultivo.ph_minimo, pulsos_riego_acumulados: @cultivo.pulsos_riego_acumulados, temperatura_actual: @cultivo.temperatura_actual, temperatura_maxima: @cultivo.temperatura_maxima, temperatura_minima: @cultivo.temperatura_minima }
    assert_redirected_to cultivo_path(assigns(:cultivo))
  end

  test "should destroy cultivo" do
    assert_difference('Cultivo.count', -1) do
      delete :destroy, id: @cultivo
    end

    assert_redirected_to cultivos_path
  end
end
