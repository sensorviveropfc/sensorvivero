class SensorViverosController < ApplicationController
  # GET /sensor_viveros
  # GET /sensor_viveros.json
  load_and_authorize_resource
  def index
    @sensor_viveros = current_user.sensor_viveros

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sensor_viveros }
    end
  end

  # GET /sensor_viveros/1
  # GET /sensor_viveros/1.json
  def show
    @sensor_vivero = SensorVivero.find(params[:id])
    

    if !@sensor_vivero.users.include? current_user
      redirect_to sensor_viveros_path
      return
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sensor_vivero }
    end
  end

  # GET /sensor_viveros/new
  # GET /sensor_viveros/new.json
  def new
    @sensor_vivero = SensorVivero.new()


    if !@sensor_vivero.users.include? current_user
      redirect_to sensor_viveros_path
      return
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sensor_vivero }
    end
  end

  # GET /sensor_viveros/1/edit
  def edit
    @sensor_vivero = SensorVivero.find(params[:id])


    if !@sensor_vivero.users.include? current_user
      redirect_to sensor_viveros_path
      return
    end
  end

  # POST /sensor_viveros
  # POST /sensor_viveros.json
  def create
    @sensor_vivero = SensorVivero.new(params[:sensor_vivero])
    @sensor_vivero.luz_encendida = false
    @sensor_vivero.riego_activo = false
    @sensor_vivero.ventilacion_encendida = false

    @default_perfil = Perfil.where(perfil: 'Propietario').first

    @pus = PerfilUserSensorVivero.new(perfil: @default_perfil, user: current_user, sensor_vivero: @sensor_vivero)
    current_user.role = 'admin'

    respond_to do |format|
      if @sensor_vivero.save

        @pus.save
        current_user.save

        format.html { redirect_to @sensor_vivero, notice: 'Sensor vivero guardado correctamente.' }
        format.json { render json: @sensor_vivero, status: :created, location: @sensor_vivero }
      else
        format.html { render action: "new" }
        format.json { render json: @sensor_vivero.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sensor_viveros/1
  # PUT /sensor_viveros/1.json
  def update
    @sensor_vivero = SensorVivero.find(params[:id])


    if !@sensor_vivero.users.include? current_user
      redirect_to sensor_viveros_path
      return
    end

    respond_to do |format|
      if @sensor_vivero.update_attributes(params[:sensor_vivero])
        format.html { redirect_to @sensor_vivero, notice: 'Sensor vivero guardado correctamente.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sensor_vivero.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sensor_viveros/1
  # DELETE /sensor_viveros/1.json
  def destroy
    @sensor_vivero = SensorVivero.find(params[:id])


    if !@sensor_vivero.users.include? current_user
      redirect_to sensor_viveros_path
      return
    end
    
    users = @sensor_vivero.users

    puts "Recuperando usuarios asociados al sensor"
    #FIXME Si no se hace este puts no recupera bien los users... users es un array vacío.
    #Quizás sea mejor pasar el destroy abajo del todo y usar la query manual (comentada)
    puts users

    puts "Borrando sensor vivero..."
    @sensor_vivero.destroy

    users.each do |u|
      puts "Comprobando usuario #{u.id}"
      #  pus = PerfilUserSensorVivero.where("sensor_vivero_id != :svid AND user_id = :uid", {svid: @sensor_vivero.id, uid: u.id})
      pus = u.sensor_viveros

      if pus.empty?
        puts "El usuario #{u.id} pierde los privilegios de admin."
        u.update_attribute(:role, 'user')
      end
    end

    respond_to do |format|
      format.html { redirect_to sensor_viveros_url }
      format.json { head :no_content }
    end
  end
end
