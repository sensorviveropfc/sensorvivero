class Reporte < ActiveRecord::Base
  attr_accessible :comentario, :fecha, :leido

  belongs_to :user
  belongs_to :cultivo
end
