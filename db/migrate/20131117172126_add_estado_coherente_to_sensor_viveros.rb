class AddEstadoCoherenteToSensorViveros < ActiveRecord::Migration
  def change
    add_column :sensor_viveros, :estado_actualizado, :boolean, :default => true
  end
end
