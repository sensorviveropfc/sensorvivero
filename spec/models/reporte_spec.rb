require 'spec_helper'

describe Reporte do

  before(:each) do
  	  vivero = FactoryGirl.create(:vivero_prueba)
      @cultivo = FactoryGirl.create(:cultivo,vivero_id: vivero.id)
      @user  = FactoryGirl.create(:admin)
  end
  it "Comprobar que no se crea un reporte sin usuario ni cultivo" do 
  	assert_raise(ActiveRecord::StatementInvalid){FactoryGirl.create(:reporte)}  	
  end
  it "Comprobar que no se crea un reporte sin usuario" do 
  	assert_raise(ActiveRecord::StatementInvalid){FactoryGirl.create(:reporte, cultivo_id: @cultivo.id)}  	
  end

  it "Comprobar que no se crea un reporte sin cultivo" do 

  	assert_raise(ActiveRecord::StatementInvalid){FactoryGirl.create(:reporte, user_id: @user.id )}  	
  end

  it "Comprobar que se crea un reporte con usuario y con cultivo" do 
	reporte = FactoryGirl.create(:reporte, user_id: @user.id,cultivo_id: @cultivo.id )
	assert_equal(@user.id, reporte.user_id)
	assert_equal(@cultivo.id, reporte.cultivo_id)  

  end

end
