class ViverosController < ApplicationController
  # GET /viveros
  # GET /viveros.json
  load_and_authorize_resource
  def index
    @viveros = current_user.viveros

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @viveros }
    end
  end

  # GET /viveros/1
  # GET /viveros/1.json
  def show
    @vivero = Vivero.find(params[:id])

    # Si el vivero no pertenece al usuario
    if !@vivero.users.include? current_user
      redirect_to viveros_path
      return
    end

    respond_to do |format|
     format.html # show.html.erb
      format.json { render json: @vivero }
    end

  end

  # GET /viveros/new
  # GET /viveros/new.json
  def new
    @vivero = Vivero.new()

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @vivero }
    end
  end

  # GET /viveros/1/edit
  def edit
    @vivero = Vivero.find(params[:id])

    # Si el vivero no pertenece al usuario
    if !@vivero.users.include? current_user
      redirect_to viveros_path
      return
    end

  end

  # POST /viveros
  # POST /viveros.json
  def create
    @vivero = Vivero.new(params[:vivero])

    @vivero.users << current_user
    
    respond_to do |format|
      if @vivero.save
        format.html { redirect_to @vivero, notice: 'Vivero guardado correctamente.' }
        format.json { render json: @vivero, status: :created, location: @vivero }
      else
        format.html { render action: "new" }
        format.json { render json: @vivero.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /viveros/1
  # PUT /viveros/1.json
  def update
    @vivero = Vivero.find(params[:id])

    # Si el vivero no pertenece al usuario
    if !@vivero.users.include? current_user
      redirect_to viveros_path
      return
    end

    respond_to do |format|
      if @vivero.update_attributes(params[:vivero])
        format.html { redirect_to @vivero, notice: 'Vivero guardado correctamente.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @vivero.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /viveros/1
  # DELETE /viveros/1.json
  def destroy
    @vivero = Vivero.find(params[:id])

    # Si el vivero no pertenece al usuario
    if !@vivero.users.include? current_user
      redirect_to viveros_path
      return
    end
    
    @vivero.destroy

    respond_to do |format|
      format.html { redirect_to viveros_url }
      format.json { head :no_content }
    end
  end
  
end
