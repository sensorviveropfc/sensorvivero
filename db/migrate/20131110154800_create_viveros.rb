class CreateViveros < ActiveRecord::Migration
  def change
    create_table :viveros do |t|
      
      t.string :nombre
      t.string :descripcion
      
      t.timestamps
    end
  end
end