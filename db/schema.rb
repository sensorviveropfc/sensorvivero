# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140317223145) do

  create_table "cultivos", :force => true do |t|
    t.string   "nombre"
    t.string   "cosecha"
    t.string   "funcionamiento"
    t.integer  "co2_actual"
    t.integer  "co2_maximo"
    t.integer  "humedad_actual"
    t.integer  "humedad_maxima"
    t.integer  "humedad_minima"
    t.integer  "duracion_pulso_riego"
    t.integer  "pulsos_riego_acumulados"
    t.integer  "maximos_pulsos_riego"
    t.integer  "luz_actual"
    t.integer  "luz_acumulada"
    t.integer  "luz_limite"
    t.integer  "fotoperiodo"
    t.integer  "ph_actual"
    t.integer  "ph_maximo"
    t.integer  "ph_minimo"
    t.integer  "temperatura_actual"
    t.integer  "temperatura_maxima"
    t.integer  "temperatura_minima"
    t.integer  "vivero_id",               :null => false
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "estadisticas", :force => true do |t|
    t.string   "descripcion"
    t.integer  "cultivo_id",  :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "medidas", :force => true do |t|
    t.integer  "temperatura"
    t.integer  "luminosidad"
    t.integer  "humedad"
    t.integer  "estadistica_id", :null => false
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "co2"
    t.integer  "ph"
  end

  create_table "models", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "models", ["email"], :name => "index_models_on_email", :unique => true
  add_index "models", ["reset_password_token"], :name => "index_models_on_reset_password_token", :unique => true

  create_table "perfil_user_sensor_viveros", :force => true do |t|
    t.integer  "perfil_id"
    t.integer  "user_id"
    t.integer  "sensor_vivero_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "perfil_user_sensor_viveros", ["perfil_id"], :name => "index_perfil_user_sensor_viveros_on_perfil_id"
  add_index "perfil_user_sensor_viveros", ["sensor_vivero_id"], :name => "index_perfil_user_sensor_viveros_on_sensor_vivero_id"
  add_index "perfil_user_sensor_viveros", ["user_id"], :name => "index_perfil_user_sensor_viveros_on_user_id"

  create_table "perfils", :force => true do |t|
    t.string   "perfil"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "reportes", :force => true do |t|
    t.string   "comentario"
    t.datetime "fecha"
    t.boolean  "leido"
    t.integer  "user_id",    :null => false
    t.integer  "cultivo_id", :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sensor_viveros", :force => true do |t|
    t.string   "numero_serie"
    t.string   "nombre"
    t.string   "descripcion"
    t.string   "ip"
    t.boolean  "luz_encendida"
    t.boolean  "riego_activo"
    t.boolean  "ventilacion_encendida"
    t.integer  "cultivo_id"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.boolean  "estado_actualizado",    :default => true
  end

  create_table "sensor_viveros_users", :id => false, :force => true do |t|
    t.integer "sensor_vivero_id"
    t.integer "user_id"
  end

  create_table "sensors", :force => true do |t|
    t.string   "tipo"
    t.integer  "sensor_vivero_id", :null => false
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",     :null => false
    t.string   "encrypted_password",     :default => "",     :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "role",                   :default => "user", :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_viveros", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "vivero_id"
  end

  create_table "viveros", :force => true do |t|
    t.string   "nombre"
    t.string   "descripcion"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_foreign_key "cultivos", "viveros", name: "cultivos_vivero_id_fk", dependent: :delete

  add_foreign_key "estadisticas", "cultivos", name: "estadisticas_cultivo_id_fk", dependent: :delete

  add_foreign_key "medidas", "estadisticas", name: "medidas_estadistica_id_fk", dependent: :delete

  add_foreign_key "perfil_user_sensor_viveros", "perfils", name: "perfil_user_sensor_viveros_perfil_id_fk", dependent: :delete
  add_foreign_key "perfil_user_sensor_viveros", "sensor_viveros", name: "perfil_user_sensor_viveros_sensor_vivero_id_fk", dependent: :delete
  add_foreign_key "perfil_user_sensor_viveros", "users", name: "perfil_user_sensor_viveros_user_id_fk", dependent: :delete

  add_foreign_key "reportes", "cultivos", name: "reportes_cultivo_id_fk", dependent: :delete
  add_foreign_key "reportes", "users", name: "reportes_user_id_fk", dependent: :delete

  add_foreign_key "sensor_viveros", "cultivos", name: "sensor_viveros_cultivo_id_fk", dependent: :nullify

  add_foreign_key "sensor_viveros_users", "sensor_viveros", name: "sensor_viveros_users_sensor_vivero_id_fk", dependent: :delete
  add_foreign_key "sensor_viveros_users", "users", name: "sensor_viveros_users_user_id_fk", dependent: :delete

  add_foreign_key "sensors", "sensor_viveros", name: "sensors_sensor_vivero_id_fk", dependent: :delete

  add_foreign_key "users_viveros", "users", name: "users_viveros_user_id_fk", dependent: :delete
  add_foreign_key "users_viveros", "viveros", name: "users_viveros_vivero_id_fk", dependent: :delete

end
