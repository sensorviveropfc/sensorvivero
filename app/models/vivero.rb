class Vivero < ActiveRecord::Base
  attr_accessible :nombre, :descripcion

  validates_uniqueness_of :nombre
  validates_presence_of :nombre
  validates_presence_of :descripcion
  
  has_many :cultivos
  has_and_belongs_to_many :users

end
