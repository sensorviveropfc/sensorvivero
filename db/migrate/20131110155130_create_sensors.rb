class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|
      t.string :tipo
      t.references :sensor_vivero, :null => false

      t.timestamps
    end

    add_foreign_key :sensors, :sensor_viveros, dependent: :delete

  end
end