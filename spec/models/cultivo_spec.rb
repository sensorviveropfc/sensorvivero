require 'spec_helper'

describe Cultivo do
	it "Comprobar que no se crea un cultivo sin vivero" do 
		assert_raise(ActiveRecord::StatementInvalid){FactoryGirl.create(:cultivo)}  	

	end

	it "Comprobar que se crea un cultivo con un vivero" do
		vivero = FactoryGirl.create(:vivero_prueba)
       	cultivo = FactoryGirl.create(:cultivo,vivero_id: vivero.id)

       	assert_equal(vivero.id, cultivo.vivero_id)

	end
end
