require 'spec_helper'

describe Medida do
	it "Comprobar que no se crea una medida sin estadistica" do 
		assert_raise(ActiveRecord::StatementInvalid){FactoryGirl.create(:medida)}  	

	end

	it "Comprobar que se crea bien una medida con una estadistica" do 
		vivero = FactoryGirl.create(:vivero_prueba)
       	cultivo = FactoryGirl.create(:cultivo,vivero_id: vivero.id)
	  	estadistica = FactoryGirl.create(:estadistica,cultivo_id: cultivo.id)
	  	medida = FactoryGirl.create(:medida, estadistica_id: estadistica.id)
	  	
	  	assert_equal(estadistica.id,medida.estadistica_id)
	end
end
