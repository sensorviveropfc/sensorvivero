class ArduinoTestMaxPulsosRiegoController < ApplicationController
  require 'json'

  def datos
    
    if params[:info]
	
      entrada = request.body.read
      info = ActiveSupport::JSON.decode(entrada)
      num_serie = info["info"]["num_serie"].to_s
      estado = info["info"]["estado"]
      
	  
	  # Establecemos las ordenes a probar
	  array = Hash.new
      array = {"max_pulsos_riego" => "15"}
      
	  Rails.cache.delete(num_serie)
      Rails.cache.write(num_serie, array)
	  puts "Cache "
	  puts Rails.cache.read(num_serie)
	  
	  # Continuamos con el procedimiento normal
      cache = Rails.cache.read(num_serie)
      
	  # Declaramos variables	  
      ack = nil
	  maxPulsosRiego = nil
      error = nil
     
	# Inicializamos con valores imposibles y/o atipicos a sobreescribir	 
	maxPulsosRiego = -1
	
      estado.each do |array|
        elemento = array["e"].to_s
        valor = array["v"].to_i
        valorS = array["v"]

        case elemento
        when 'NumSerie'
          ack = true
        when 'MaxPulsosRiego'
          maxPulsosRiego = valor
        when 'error'
          error = valorS
        end        
      end
      
      if ack
        #Arduino ha ejecutado las acciones. Comprobamos los resultados.
		
		# Extraemos el resultado esperado
		maxPulsosRiegoEsperado = cache["max_pulsos_riego"]
		
		if maxPulsosRiego == -1 or maxPulsosRiego != maxPulsosRiegoEsperado.to_i
			puts "Test Failure: setMaxPulsosRiegos not working."
		else
			puts "Test OK: setMaxPulsosRiegos working."
		end

		Rails.cache.delete(num_serie)
      else
       
        if cache
          respuesta = getRespuesta(num_serie, cache)
          puts respuesta
          render :json => { :acc => respuesta}
        end
      end
      
      if error
        puts "Test Failure. Ha ocurrido un error:"
        puts error.to_s
      end
      
    else
      render :json => {:mensaje => "Reenviar datos"}
    end
  end

 def getRespuesta(num_serie, cache)
    
	modo = cache["modo"]
	temperatura = cache["temperatura"]
	luz = cache["luz"]
	riego = cache["riego"]    
	fotoperiodo = cache["fotoperiodo"]
	luz_limite = cache["luz_limite"]
	temp_minima = cache["temperatura_minima"]
	temp_maxima = cache["temperatura_maxima"]
	max_pulsos_riego = cache["max_pulsos_riego"]
	humedad_minima = cache["humedad_minima"]
	humedad_maxima = cache["humedad_maxima"]  
      
      resultado = Array.new
      if modo
        if modo == 'auto'
          resultado << switchModeAuto
          return resultado
        elsif modo == 'manual'
          resultado << switchModeManual
        else
          resultado << switchModeAsist
        end
      end
  
      if luz
        if luz.to_i == 1
          resultado << encenderLuz
        else
          resultado << apagarLuz
        end
      end
      
      if temperatura
        if temperatura.to_i == 1
          resultado << encenderTemp
        else
          resultado << apagarTemp
        end
      end
  
      if riego
        if riego.to_i > 0
          resultado << encenderRiego
          for i in (1..riego.to_i).step(10)
            resultado << delay10000
          end
          resultado << apagarRiego
        end
      end 

      if fotoperiodo
        resultado << setFotoPeriodo(fotoperiodo)
      end
      
      if luz_limite
        resultado << setLumMinMax(luz_limite, luz_limite)
      end
      
      if temp_minima and temp_maxima
        resultado << setTempMinMax(temp_minima,temp_maxima)
      end
      
      if max_pulsos_riego 
        resultado << setMaxPulsosRiegos(max_pulsos_riego)
      end
      
      if humedad_minima and humedad_maxima 
        resultado << setHumMinMax(humedad_minima,humedad_maxima)
      end            
    return resultado
  end
  
  def switchModeManual
    return { :a => 0, :v1 => "m" }
  end

  def switchModeAsist
    return { :a => 0 , :v1 => "s" }
  end

  def switchModeAuto
    return { :a => 0, :v1 => "x" }
  end

  def getData
    return { :a => 1 }
  end

  def encenderRiego
    return { :a => 2 }
  end

  def encenderLuz
    return { :a => 3 }
  end

  def encenderTemp
    return { :a => 4 }
  end

  def apagarRiego
    return { :a => 5 }
  end

  def apagarLuz
    return { :a => 6 }
  end

  def apagarTemp
    return { :a => 7 }
  end

  def getLumMinMax
    return { :a => 8 }
  end

  def getTempMinMax
    return { :a => 9 }
  end

  def getHumMinMax
    return { :a => 10 }
  end

  def getMaxPulsosRiegos
    return { :a => 11 }
  end

  #Hist => Histeresis
  def getLumHist
    return { :a => 12 }
  end

  def getFotoPeriodo
    return { :a => 13 }
  end

  def getHoraReset
    return { :a => 14 }
  end

  def setLumMinMax(val,val2)
    return { :a => 15 ,:v1 => val, :v2 => val2 }
  end

  def setTempMinMax(val,val2)
    return { :a => 16 ,:v1 => val, :v2 => val2 }
  end

  def setHumMinMax(val,val2)
    return { :a => 17 ,:v1 => val, :v2 => val2 }
  end

  def setMaxPulsosRiegos(val)
    return { :a => 18,:v1 => val }
  end

  def setLumHist(val)
    return { :a => 19 ,:v1 => val }
  end

  def setFotoPeriodo(val)
    return { :a => 20 ,:v1 => val }
  end

  def setHoraReset(hora,minuto)
    return { :a => 21,:v1 => hora, :v2 => minuto }
  end

  def getNumSerie
    return { :a => 22 }
  end

  def delay10000
    return { :a => 23 ,:v1 => 10000 }
  end

  def nop
    return { :a => 24 }
  end

end
