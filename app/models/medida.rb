class Medida < ActiveRecord::Base
  attr_accessible :humedad, :luminosidad, :temperatura, :co2, :ph

  belongs_to :estadistica
end
