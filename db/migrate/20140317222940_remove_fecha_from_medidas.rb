class RemoveFechaFromMedidas < ActiveRecord::Migration
  def up
    remove_column :medidas, :fecha
  end

  def down
    add_column :medidas, :fecha, :datetime
  end
end
