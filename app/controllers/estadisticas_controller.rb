class EstadisticasController < ApplicationController
  # GET /estadisticas
  # GET /estadisticas.json
  #load_and_authorize_resource
  def index
    @estadisticas = Estadistica.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @estadisticas }
    end
  end

  # GET /estadisticas/1
  # GET /estadisticas/1.json
  def show
    @estadistica = Estadistica.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @estadistica }
    end
  end

  # GET /estadisticas/new
  # GET /estadisticas/new.json
  def new
    @estadistica = Estadistica.new
    @cultivos = Vivero.joins(:users).where("users.id = :userid", userid: current_user.id).map{ |group| [group.nombre, group.cultivos.includes(:sensor_vivero).map{ |c| [c.nombre, c.id] } ] }
    @cultivo_selected = nil

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @estadistica }
    end
  end

  # GET /estadisticas/1/edit
  def edit
    @estadistica = Estadistica.find(params[:id])
    @cultivos = Vivero.joins(:users).where("users.id = :userid", userid: current_user.id).map{ |group| [group.nombre, group.cultivos.includes(:sensor_vivero).map{ |c| [c.nombre, c.id] } ] }
    @cultivo_selected = @estadistica.cultivo.id
  end

  # POST /estadisticas
  # POST /estadisticas.json
  def create
    @estadistica = Estadistica.new(params[:estadistica])

    respond_to do |format|
      if @estadistica.save
        format.html { redirect_to @estadistica, notice: 'Estadistica guardado correctamente.' }
        format.json { render json: @estadistica, status: :created, location: @estadistica }
      else
        format.html { render action: "new" }
        format.json { render json: @estadistica.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /estadisticas/1
  # PUT /estadisticas/1.json
  def update
    @estadistica = Estadistica.find(params[:id])

    respond_to do |format|
      if @estadistica.update_attributes(params[:estadistica])
        format.html { redirect_to @estadistica, notice: 'Estadistica guardado correctamente.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @estadistica.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /estadisticas/1
  # DELETE /estadisticas/1.json
  def destroy
    @estadistica = Estadistica.find(params[:id])
    @estadistica.destroy

    respond_to do |format|
      format.html { redirect_to estadisticas_url }
      format.json { head :no_content }
    end
  end
end
