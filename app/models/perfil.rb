class Perfil < ActiveRecord::Base
  attr_accessible :perfil
  
  has_many :perfil_user_sensor_viveros
  has_many :users, :through => :perfil_user_sensor_viveros
  has_many :sensor_viveros, :through => :perfil_user_sensor_viveros
  
end
