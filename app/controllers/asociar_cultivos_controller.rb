class AsociarCultivosController < ApplicationController
  # GET /asociar_cultivos
  # GET /asociar_cultivos.json
  
  def index
    @sensor_viveros = SensorVivero.joins(:users, :perfils).where("users.id = :userid AND perfils.perfil in (:perfiles) AND sensor_viveros.cultivo_id is NULL", userid: current_user.id, perfiles: ['Propietario', 'Agricultor']).map{ |x| [x.nombre, x.id]}

    @cultivos = Vivero.joins(:users).where("users.id = :userid", userid: current_user.id).map{ |group| [group.nombre, group.cultivos.includes(:sensor_vivero).where(sensor_viveros: {cultivo_id: nil}).map{ |c| [c.nombre, c.id] } ] }

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sensor_viveros }
    end
  end

  # PUT /asociar_cultivos
  # PUT /asociar_cultivos.json
  def update
    @sensor_vivero = SensorVivero.find(params[:asociar][:sensor_vivero_id])
    @cultivo = Cultivo.find(params[:asociar][:cultivo_id])

    respond_to do |format|
      if @sensor_vivero.update_attribute(:cultivo_id, @cultivo.id)
        format.html { redirect_to @sensor_vivero, notice: 'Sistema asociado correctamente.'}
        format.json { head :no_content }
      else
        format.html { redirect_to @sensor_vivero, notice: 'Ha ocurrido un error.' }
        format.json { head :no_content }
      end
    end
  end
end
