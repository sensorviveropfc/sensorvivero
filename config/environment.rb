# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
SistemaVivero::Application.initialize!
ENV['RAILS_ENV'] ||= 'production'	#Descomentar para poner en modo producción

