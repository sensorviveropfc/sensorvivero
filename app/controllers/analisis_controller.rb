require 'gchart'
require 'tempfile'

class AnalisisController < ApplicationController
  
  def index
    @cultivo_medidas = Hash.new
    sensores_propios = SensorVivero.joins(:users, :perfils).where("users.id = :userid AND perfils.perfil in (:perfiles) AND sensor_viveros.cultivo_id is not NULL", userid: current_user.id, perfiles: ['Propietario', 'Agricultor', 'Supervisor'])
    sensores_propios.each do |sensor|
      cultivo = sensor.cultivo
      if cultivo
       estadisticas =  Estadistica.where("cultivo_id = :cultivo_id",cultivo_id: cultivo.id);
        if estadisticas.any?
          medidas = Array.new
          estadisticas.each {|estadistica| medidas.concat estadistica.medidas}
          @cultivo_medidas[cultivo.nombre] = medidas
        end
      end
    end
  end
  
end