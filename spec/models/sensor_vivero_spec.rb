require 'spec_helper'

describe SensorVivero do
  it "Comprobar que no se crea un sensorvivero sin campos" do
  	assert_raise(ActiveRecord::RecordInvalid){FactoryGirl.create(:sensor_vivero)}
  end
  it "Comprobar que no se crea un sensorvivero sin nombre" do
  	assert_raise(ActiveRecord::RecordInvalid){FactoryGirl.create(:sensor_vivero, numero_serie: "svn001",descripcion: "descripcion prueba")}
  end

  it "Comprobar que no se crea un sensorvivero sin descripcion" do
  	assert_raise(ActiveRecord::RecordInvalid){FactoryGirl.create(:sensor_vivero, numero_serie: "svn001", nombre: "nombre prueba")}
  end


  it "Comprobar que no se crea un sensorvivero sin numero de serie" do
  	assert_raise(ActiveRecord::RecordInvalid){FactoryGirl.create(:sensor_vivero, nombre: "nombre prueba", descripcion: "descripcion prueba")}
  end

    it "Comprobar que no se crean dos sensorviveros con el mismo numero de serie" do
  	assert_raise(ActiveRecord::RecordInvalid){
     FactoryGirl.create(:sensor_vivero, numero_serie: "svn001", nombre: "nombre prueba", descripcion: "descripcion prueba")
     FactoryGirl.create(:sensor_vivero,numero_serie: "svn001",  nombre: "nombre prueba", descripcion: "descripcion prueba2")

  	}
  end

  it "Comprobar que se crea un sensorvivero con todos los campos" do
  	sensorvivero = FactoryGirl.create(:sensor_vivero, numero_serie: "svn001", nombre: "nombre prueba", descripcion: "descripcion prueba")
  	assert_equal("svn001", sensorvivero.numero_serie)
  	assert_equal("nombre prueba", sensorvivero.nombre)
	  assert_equal("descripcion prueba", sensorvivero.descripcion)

  end
end
