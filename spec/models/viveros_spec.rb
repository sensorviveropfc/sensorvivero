require 'spec_helper'

describe "Viveros" do
  it "Comprobar que no se crea un vivero sin nombre ni descripcion" do
  	assert_raise(ActiveRecord::RecordInvalid){FactoryGirl.create(:vivero)}
  end
  it "Comprobar que no se crea un vivero sin nombre" do
  	assert_raise(ActiveRecord::RecordInvalid){FactoryGirl.create(:vivero, descripcion: "descripcion prueba")}
  end

  it "Comprobar que no se crea un vivero sin descripcion" do
  	assert_raise(ActiveRecord::RecordInvalid){FactoryGirl.create(:vivero, nombre: "nombre prueba")}
  end

    it "Comprobar que no se crean dos viveros con el mismo nombre" do
  	assert_raise(ActiveRecord::RecordInvalid){
     FactoryGirl.create(:vivero, nombre: "nombre prueba", descripcion: "descripcion prueba")
     FactoryGirl.create(:vivero, nombre: "nombre prueba", descripcion: "descripcion prueba2")

  	}
  end

  it "Comprobar que se crea un vivero con todos los campos" do
  	vivero = FactoryGirl.create(:vivero, nombre: "nombre prueba", descripcion: "descripcion prueba")
  	assert_equal("nombre prueba", vivero.nombre)
	  assert_equal("descripcion prueba", vivero.descripcion)

  end
end
