class CreateCultivos < ActiveRecord::Migration
  def change
    create_table :cultivos do |t|
      
      t.string :nombre
      t.string :cosecha
      t.string :funcionamiento   
      t.integer :co2_actual
      t.integer :co2_maximo
      t.integer :humedad_actual
      t.integer :humedad_maxima
      t.integer :humedad_minima
      t.integer :duracion_pulso_riego
      t.integer :pulsos_riego_acumulados
      t.integer :maximos_pulsos_riego     
      t.integer :luz_actual
      t.integer :luz_acumulada
      t.integer :luz_limite
      t.integer :fotoperiodo
      t.integer :ph_actual
      t.integer :ph_maximo
      t.integer :ph_minimo    
      t.integer :temperatura_actual
      t.integer :temperatura_maxima
      t.integer :temperatura_minima
      
      t.references :vivero, :null => false
      
      t.timestamps
    end
    
    add_foreign_key :cultivos, :viveros, dependent: :delete
    
  end
end