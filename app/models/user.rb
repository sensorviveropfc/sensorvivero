class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body
  validates :email, :uniqueness => true
  validates_format_of :email, :with => /^([^\s]+)((?:[-a-z0-9]\.)[a-z]{2,})$/i
  
  has_many :perfil_user_sensor_viveros
  has_many :sensor_viveros, :through => :perfil_user_sensor_viveros
  has_many :perfils, :through => :perfil_user_sensor_viveros
  
  has_and_belongs_to_many :viveros
  has_many :reportes

  attr_protected ROLES = %w[supervisor agricultor admin]
  attr_protected SHOWED_ROLES = %w[supervisor agricultor admin]


end
