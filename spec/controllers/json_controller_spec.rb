require 'spec_helper'

describe JsonController do
  def valid_attributes
    {  }
  end
 def valid_session
    {}
  end

  before(:each) do
      @sensor_vivero = FactoryGirl.create(:sensor_vivero_pruebas)
  end

  describe "POST datos" do
       
    context 'json invalido' do
   		 it "json vacio" do
   		     json = { :format => 'json', :application => { } }
   		     post :datos, json
		
   		     JSON.parse(response.body)["mensaje"].should  eq("Reenviar datos")
   		 end

   		 it "json sin el parametro info" do
   		 	 json = { :format => 'json', :application => { :info2 => {} }}
   		     post :datos, json
		
   		     JSON.parse(response.body)["mensaje"].should  eq("Reenviar datos")
   		 end

   		it "json con info vacio" do
         json = { :format => 'json', :application => { :info => {} }}
         post :datos, json
   		   JSON.parse(response.body)["mensaje"].should  eq("Reenviar datos")
   		 end

      it "json correcto " do 
        json = { :format => 'json', :application => { :info => {:num_serie => "SVP-0001", 
          :estado => [{:elemento => "Luminosidad",:valor => 1016}] } }}
         post :datos, json
         JSON.parse(response.body)["mensaje"].should  eq("Reenviar datos")
      end

  	end
  end


end
