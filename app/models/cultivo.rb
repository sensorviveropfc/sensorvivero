class Cultivo < ActiveRecord::Base
  attr_accessible :sensor_vivero_id,:co2_actual, :co2_maximo, 
  	:cosecha, :duracion_pulso_riego, :fotoperiodo, :funcionamiento, 
  	:humedad_actual, :humedad_maxima, :humedad_minima, 
  	:luz_actual, :luz_acumulada, :luz_limite,
  	 :maximos_pulsos_riego, :nombre, :ph_actual, :ph_maximo,
  	  :ph_minimo, :pulsos_riego_acumulados, :temperatura_actual, 
  	  :temperatura_maxima, :temperatura_minima, :vivero_id

#TODO inicializar maximo_pulsos_acumulados etc etc

  has_many :estadisticas
  has_many :reportes
  has_one :sensor_vivero
  belongs_to :vivero 

end
