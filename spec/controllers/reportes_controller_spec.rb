require 'spec_helper'

describe ReportesController do
def valid_attributes
    {  }
  end
 def valid_session
    {}
  end

  before(:each) do
  	  @vivero = FactoryGirl.create(:vivero_prueba)
  	  @cultivo = FactoryGirl.create(:cultivo,vivero_id: @vivero.id)
      @user = FactoryGirl.create(:user)
      @user_propietario = FactoryGirl.create(:admin, email:  "admin_propietario@prueba.com")

      @reporte = FactoryGirl.create(:reporte, user_id: @user_propietario.id,cultivo_id: @cultivo.id )

      @admin = FactoryGirl.create(:admin)
  end

  describe "GET index" do
    it "role admin" do
      login(@admin)
      get :index, {}, valid_session
      response.should be_success
    end
    it "role user" do
     login(@user)
      get :index, {}, valid_session
      response.should be_success
  	end
  end

  describe "GET show" do 
  	it "sin id" do
      login(@user)
      lambda {get :show, {}, valid_session}.should raise_error(ActiveRecord::RecordNotFound)
    end
  	it "id incorrecto" do
      login(@user)
      lambda {get :show, {:id => 4568}, valid_session}.should raise_error(ActiveRecord::RecordNotFound)
    end

    it "role user" do
     login(@user)
      get :show, {:id => @reporte.id}, valid_session
      response.should redirect_to(reportes_path)
  	end

    it "role admin sin pertenecer al vivero" do 
      login(@admin)
      get :show, {:id => @reporte.id}, valid_session
      response.should redirect_to(reportes_path)
    end

    it "role admin perteneciendo al vivero" do 
      login(@user_propietario)
      get :show, {:id => @reporte.id}, valid_session
      response.should be_success
    end

  end
end
