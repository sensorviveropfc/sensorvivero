class CreateSensorViveros < ActiveRecord::Migration
  def change
    create_table :sensor_viveros do |t|
      t.string :numero_serie
      t.string :nombre
      t.string :descripcion
      t.string :ip
      t.boolean :luz_encendida
      t.boolean :riego_activo
      t.boolean :ventilacion_encendida
      t.references :cultivo

      t.timestamps
    end

    add_foreign_key :sensor_viveros, :cultivos, dependent: :nullify

  end
end