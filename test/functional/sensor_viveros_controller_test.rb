require 'test_helper'

class SensorViverosControllerTest < ActionController::TestCase
  setup do
    @sensor_vivero = sensor_viveros(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sensor_viveros)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sensor_vivero" do
    assert_difference('SensorVivero.count') do
      post :create, sensor_vivero: { descripcion: @sensor_vivero.descripcion, ip: @sensor_vivero.ip, luz_encendida: @sensor_vivero.luz_encendida, nombre: @sensor_vivero.nombre, numero_serie: @sensor_vivero.numero_serie, riego_activo: @sensor_vivero.riego_activo, ventilacion_encendida: @sensor_vivero.ventilacion_encendida }
    end

    assert_redirected_to sensor_vivero_path(assigns(:sensor_vivero))
  end

  test "should show sensor_vivero" do
    get :show, id: @sensor_vivero
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sensor_vivero
    assert_response :success
  end

  test "should update sensor_vivero" do
    put :update, id: @sensor_vivero, sensor_vivero: { descripcion: @sensor_vivero.descripcion, ip: @sensor_vivero.ip, luz_encendida: @sensor_vivero.luz_encendida, nombre: @sensor_vivero.nombre, numero_serie: @sensor_vivero.numero_serie, riego_activo: @sensor_vivero.riego_activo, ventilacion_encendida: @sensor_vivero.ventilacion_encendida }
    assert_redirected_to sensor_vivero_path(assigns(:sensor_vivero))
  end

  test "should destroy sensor_vivero" do
    assert_difference('SensorVivero.count', -1) do
      delete :destroy, id: @sensor_vivero
    end

    assert_redirected_to sensor_viveros_path
  end
end
