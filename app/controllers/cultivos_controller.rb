class CultivosController < ApplicationController
  # GET /cultivos
  # GET /cultivos.json
  load_and_authorize_resource
  def index
    @cultivos = Cultivo.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cultivos }
    end
  end

  # GET /cultivos/1
  # GET /cultivos/1.json
  def show
    @cultivo = Cultivo.find(params[:id])

   if !@cultivo.vivero.users.include? current_user
      redirect_to cultivos_path
      return
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cultivo }
    end
  end

  # GET /cultivos/new
  # GET /cultivos/new.json
  #Create a Culvito if there is a Vivero, if none exists redirected to a Vivero and news shows: 'Tiene que crear un vivero antes'
  def new
    @cultivo = Cultivo.new
    @viveros = current_user.viveros.map{ |x| [x.nombre, x.id]}
    puts @viveros
    respond_to do |format|
      if @viveros.any?
        format.html # new.html.erb
        format.json { render json: @cultivo }
      else
         format.html { redirect_to controller: :viveros, action: :new, notice: 'Tiene que crear un vivero antes'}
         format.json { render json: @cultivo }
      end
    end
  end

  # GET /cultivos/1/edit
  def edit
    @cultivo = Cultivo.find(params[:id])
    if !@cultivo.vivero.users.include? current_user
      redirect_to cultivos_path
      return
    end
    @viveros = current_user.viveros.map{ |x| [x.nombre, x.id]}
  end

  # POST /cultivos
  # POST /cultivos.json
  def create
    @cultivo = Cultivo.new(params[:cultivo])
    @cultivo.pulsos_riego_acumulados = 0

    respond_to do |format|
      if @cultivo.save
        format.html { redirect_to @cultivo, notice: 'Cultivo guardado correctamente.' }
        format.json { render json: @cultivo, status: :created, location: @cultivo }
      else
        format.html { render action: "new" }
        format.json { render json: @cultivo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /cultivos/1
  # PUT /cultivos/1.json
  def update
    @cultivo = Cultivo.find(params[:id])
   if !@cultivo.vivero.users.include? current_user
      redirect_to cultivos_path
      return
    end
    respond_to do |format|
      if @cultivo.update_attributes(params[:cultivo])
        format.html { redirect_to @cultivo, notice: 'Cultivo guardado correctamente.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cultivo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cultivos/1
  # DELETE /cultivos/1.json
  def destroy
    @cultivo = Cultivo.find(params[:id])
    if !@cultivo.vivero.users.include? current_user
      redirect_to cultivos_path
      return
    end
    @cultivo.destroy

    respond_to do |format|
      format.html { redirect_to cultivos_url }
      format.json { head :no_content }
    end
  end
end
