class DesasociarSvUsersController < ApplicationController
  # GET /desasociar_sv_users
  # GET /desasociar_sv_users.json
  def index
    @sistemas_viveros = SensorVivero.joins(:users, :perfils).where("users.id = :userid AND perfils.perfil = :perfil", userid: current_user.id, perfil: 'Propietario').map{ |x| [x.nombre, x.id]}

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sistemas_viveros }
    end
  end

  # PUT /desasociar_sv_users
  # PUT /desasociar_sv_users.json
  def update
    @sistema_vivero = SensorVivero.find(params[:desasociar_sv_user][:sistema_vivero_id])
    @users = User.where("users.email = :mail", mail: params[:desasociar_sv_user][:user_mail])

    respond_to do |format|

      if @users.empty?
        format.html { redirect_to sensor_viveros_path, notice: 'El usuario no existe.' }
        format.json { head :no_content }
      elsif @users[0] == current_user
        format.html { redirect_to sensor_viveros_path, notice: 'No puede cambiar sus propios permisos.' }
        format.json { head :no_content }
      else
        @perfiles_anteriores = PerfilUserSensorVivero.where("user_id = :user AND sensor_vivero_id = :sv", user: @users[0], sv: @sistema_vivero)

        if @perfiles_anteriores.empty?
          format.html { redirect_to sensor_viveros_path, notice: 'El usuario no tiene permisos sobre el sistema.'}
          format.json { head :no_content }
        else
          @perfiles_anteriores.each do |e|
            e.destroy
          end
          format.html { redirect_to sensor_viveros_path, notice: 'Permisos de usuario eliminados correctamente.'}
          format.json { head :no_content }
        end
      end

    end
  end

end
