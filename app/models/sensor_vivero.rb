class SensorVivero < ActiveRecord::Base
  attr_accessible :descripcion, :ip, :luz_encendida, :nombre, :numero_serie, :riego_activo, :ventilacion_encendida, :cultivo_id, :estado_actualizado

  validates_uniqueness_of :numero_serie
  validates_presence_of :numero_serie
  validates_presence_of :nombre
  validates_presence_of :descripcion

  has_many :sensors
  belongs_to :cultivo

  has_many :perfil_user_sensor_viveros
  has_many :users, :through => :perfil_user_sensor_viveros
  has_many :perfils, :through => :perfil_user_sensor_viveros

end
