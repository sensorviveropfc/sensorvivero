# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sensor_vivero do
  end

  	factory :sensor_vivero_pruebas, class: 'SensorVivero' do 
		numero_serie	"vpn-prueba"
   		nombre			"prueba"
   		descripcion 	"descripcion prueba"
	end
end
