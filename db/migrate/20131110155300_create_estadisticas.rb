class CreateEstadisticas < ActiveRecord::Migration
  def change
    create_table :estadisticas do |t|
      t.string :descripcion
      t.references :cultivo, :null => false

      t.timestamps
    end
    
    add_foreign_key :estadisticas, :cultivos, dependent: :delete
    
  end
end