class AsociarViverosController < ApplicationController
  # GET /asociar_viveros
  # GET /asociar_viveros.json
  def index
    @viveros = Vivero.joins(:users).where("users.id = :userid AND users.role = :role", userid: current_user.id, role: 'admin').map{ |x| [x.nombre, x.id]}

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @viveros }
    end
  end

  # PUT /asociar_viveros
  # PUT /asociar_viveros.json
  def update
    @vivero = Vivero.find(params[:asociar_vivero][:vivero_id])
    @users = User.where("users.email = :mail", mail: params[:asociar_vivero][:user_mail])

    respond_to do |format|

      if @users.empty?
        format.html { redirect_to viveros_path, notice: 'El usuario no existe.' }
        format.json { head :no_content }
      elsif @vivero.users.include?(@users[0])
        format.html { redirect_to viveros_path, notice: 'El usuario ya estaba asociado al vivero.' }
        format.json { head :no_content }
      else
        @vivero.users << @users[0]
        if @vivero.save
          format.html { redirect_to viveros_path, notice: 'Usuario asociado al vivero correctamente.'}
          format.json { head :no_content }
        else
          format.html { redirect_to viveros_path, notice: 'Ha ocurrido un error.' }
          format.json { head :no_content }
        end
      end

    end
  end

end
