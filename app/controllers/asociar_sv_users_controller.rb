class AsociarSvUsersController < ApplicationController
  # GET /asociar_sv_users
  # GET /asociar_sv_users.json
  def index
    @sistemas_viveros = SensorVivero.joins(:users, :perfils).where("users.id = :userid AND perfils.perfil = :perfil", userid: current_user.id, perfil: 'Propietario').map{ |x| [x.nombre, x.id]}
    @perfils = Perfil.all.map{ |x| [x.perfil, x.id]  }
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sistemas_viveros }
    end
  end

  # PUT /asociar_sv_users
  # PUT /asociar_sv_users.json
  def update
    @sistema_vivero = SensorVivero.find(params[:asociar_sv_user][:sistema_vivero_id])
    @users = User.where("users.email = :mail", mail: params[:asociar_sv_user][:user_mail])
    @perfil = Perfil.find(params[:asociar_sv_user][:perfil_id])

    @pus = PerfilUserSensorVivero.new
    @pus.perfil = @perfil
    @pus.sensor_vivero = @sistema_vivero
    @pus.user = @users[0]

    respond_to do |format|

      if @users.empty?
        format.html { redirect_to sensor_viveros_path, notice: 'El usuario no existe.' }
        format.json { head :no_content }
      elsif @users[0] == current_user
        format.html { redirect_to sensor_viveros_path, notice: 'No puede cambiar sus propios permisos.' }
        format.json { head :no_content }
      #      elsif @sistema_vivero.users.include?(@users[0])
      #        format.html { redirect_to sensor_viveros_path, notice: 'El usuario ya estaba asociado al sistema.' }
      #        format.json { head :no_content }
      else
        if @pus.save
          @perfiles_anteriores = PerfilUserSensorVivero.where("user_id = :user AND sensor_vivero_id = :sv", user: @users[0], sv: @sistema_vivero)
          @perfiles_anteriores.each do |e|
            unless e == @pus
              e.destroy
            end
          end
          if @perfiles_anteriores.length == 1
            format.html { redirect_to sensor_viveros_path, notice: 'Permisos de usuario asignados correctamente.'}
            format.json { head :no_content }
          else
            format.html { redirect_to sensor_viveros_path, notice: 'Permisos de usuario cambiados correctamente.'}
            format.json { head :no_content }
          end
        else
          format.html { redirect_to sensor_viveros_path, notice: 'Ha ocurrido un error.' }
          format.json { head :no_content }
        end
      end

    end
  end

end
