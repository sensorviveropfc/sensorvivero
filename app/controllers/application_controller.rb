class ApplicationController < ActionController::Base
  protect_from_forgery

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = "Acceso denegado!"
    redirect_to root_url
  end

  before_filter :instantiateUser

  def instantiateUser
    @user = User.new
  end

  def after_sign_in_path_for(resource)
    '/control/principal '
  end

end
