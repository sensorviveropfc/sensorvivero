class Estadistica < ActiveRecord::Base
  attr_accessible :descripcion, :cultivo_id

  has_many :medidas
  belongs_to :cultivo
end
