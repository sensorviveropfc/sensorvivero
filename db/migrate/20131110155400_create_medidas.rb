class CreateMedidas < ActiveRecord::Migration
  def change
    create_table :medidas do |t|
      t.integer :temperatura
      t.integer :luminosidad
      t.integer :humedad
      t.datetime :fecha
      t.references :estadistica, :null => false

      t.timestamps
    end

    add_foreign_key :medidas, :estadisticas, dependent: :delete

  end
end