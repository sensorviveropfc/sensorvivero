class CreatePerfilUserSensorViveros < ActiveRecord::Migration
  def change
    create_table :perfil_user_sensor_viveros do |t|
      t.references :perfil
      t.references :user
      t.references :sensor_vivero

      t.timestamps
    end
    add_index :perfil_user_sensor_viveros, :perfil_id
    add_index :perfil_user_sensor_viveros, :user_id
    add_index :perfil_user_sensor_viveros, :sensor_vivero_id

    add_foreign_key :perfil_user_sensor_viveros, :perfils, dependent: :delete
    add_foreign_key :perfil_user_sensor_viveros, :users, dependent: :delete
    add_foreign_key :perfil_user_sensor_viveros, :sensor_viveros, dependent: :delete

  end
end
