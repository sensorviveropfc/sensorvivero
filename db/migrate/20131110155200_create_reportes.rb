class CreateReportes < ActiveRecord::Migration
  def change
    create_table :reportes do |t|
      t.string :comentario
      t.datetime :fecha
      t.boolean :leido
      t.references :user, :null => false
      t.references :cultivo, :null => false

      t.timestamps
    end

    add_foreign_key :reportes, :users, dependent: :delete
    add_foreign_key :reportes, :cultivos, dependent: :delete

  end
end