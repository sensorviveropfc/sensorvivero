class UserMailer < ActionMailer::Base
default :from => "sensorvivero@gmail.com"

  def send_email(email,subject, body)
    mail(:to => email, :subject => subject, :body => body)
  end

end
