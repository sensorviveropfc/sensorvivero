require 'spec_helper'

describe CultivosController do
def valid_attributes
    {  }
  end
 def valid_session
    {}
  end

  before(:each) do
  	  @vivero = FactoryGirl.create(:vivero_prueba)
  	  @cultivo = FactoryGirl.create(:cultivo,vivero_id: @vivero.id)
      @user = FactoryGirl.create(:user)
      @admin = FactoryGirl.create(:admin)
  end

  describe "GET index" do
    it "role admin" do
      login(@admin)
      get :index, {}, valid_session
      response.should be_success
    end
    it "role user" do
     login(@user)
      get :index, {}, valid_session
      response.should be_success
  	end
  end

  describe "GET show" do 
  	it "sin id" do
      login(@user)
      lambda {get :show, {}, valid_session}.should raise_error(ActiveRecord::RecordNotFound)
    end
  	it "id incorrecto" do
      login(@user)
      lambda {get :show, {:id => 4568}, valid_session}.should raise_error(ActiveRecord::RecordNotFound)
    end

    it "role user" do
     login(@user)
      get :show, {:id => @cultivo.id}, valid_session
      response.should redirect_to(cultivos_path)
  	end

    it "role admin sin pertenecer al vivero" do 
      login(@admin)
      get :show, {:id => @cultivo.id}, valid_session
      response.should redirect_to(cultivos_path)
    end

    it "role admin perteneciendo al vivero" do 
      login(@admin)
      @vivero.users << @admin
      @vivero.save
      get :show, {:id => @cultivo.id}, valid_session
      response.should be_success
    end

  end
end
