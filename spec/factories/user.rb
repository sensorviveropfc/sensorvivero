FactoryGirl.define  do
  factory :user do 
    email  "user@prueba.com"
    password 12345678
    password_confirmation 12345678
    role "user"
  end

  factory :admin, class: 'User'  do 
    email  "admin@prueba.com"
    password 12345678
    password_confirmation 12345678
    role "admin"
  end

end